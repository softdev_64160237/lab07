/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.lab07;

import java.util.ArrayList;

/**
 *
 * @author EliteCorps
 */
public class UserManagement {
    public static void main(String[] args) {
        User admin = new User("admin", "Administrator", "Pass@1234", 'M', 'A');
        User user1 = new User("user1", "User 1", "Pass@1234", 'M', 'U');
        User user2 = new User("user2", "User 2", "Pass@1234", 'M', 'U');
        
        ArrayList<User> userList = new ArrayList<>();
        
        userList.add(admin);
        userList.add(user1);
        userList.add(user2);
        
        for (User user : userList) {
            System.out.println(user);
        }
    }
}
